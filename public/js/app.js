var shots = angular.module('shots', ['ui.ace']);

var Account = function(){
	this.name = "";
	this.lname = "";
	this.fname = "";
	this.mail = "";
};

var Notebook = function(){
	this.name = "";
	this.author = "";
	this.createdAt = {};
    this.updatedAt = {};
    this.isPublic = false;
    this.color = 'gray';
	this.notes = [];
};

var Note = function(){
  this.title = "";
  this.author = "";
  this.notebook = "";
  this.tags = [];
  this.createdAt = {};
  this.updatedAt = {};
  this.isPublic = false;
  this.contentList = [];
};

var ContentBlock = function(){
  this.blockType = "";
  this.blockData = "";
  this.blockLanguage = "";
};

shots.factory("global",
    function ($rootScope, $http) {

    	var obj = {};
    	obj.token = "";
    	obj.uid = "";
    	obj.account = {};
    	obj.notebooksList = [];
        obj.noteList = [];
        obj.lastNote = {};

    	obj.createAccount = function(mail, password, firstName, lastName){
    		$http({
    			url: "http://" + location.host+ "/accounts/signup" ,
        		method: "POST",
        		data: { 'mail' : mail , 'password' : password, 'firstName' : firstName, 'lastName' : lastName}
    		}).success(function (data, status, headers, config) {
        		console.log("Guardo el Usuario");
            	console.log(data);
    		}).error(function (data, status, headers, config) {
        		console.log("No guardo el Usuario");
    			console.log(data);
    		});
    	};

    	obj.login = function(mail, password){
    		if (obj.token === "" || obj.uid === ""){
    			$http({
    				url: "http://" + location.host + "/accounts/authenticate" ,
        			method: "POST",
        			data: { 'mail' : mail , 'password' : password }
    			}).success(function (data, status, headers, config) {
        			obj.token = data.token;
        			obj.uid = data.uid;
                    $rootScope.$broadcast("login");
        			console.log(obj.token);
    			}).error(function (data, status, headers, config) {
        			console.log("Login refuse");
    				console.log(data);
    			});
    		}
            else{
                $rootScope.$broadcast("login");
            }

    	};

    	obj.getUser = function(){
    		if (angular.equals({}, obj.account)) {    			
    			$http({
    				url: "http://" + location.host + "/api/myaccount/",
        			method: "GET",
        			headers: { 'Authorization' : obj.token , 'uid' : obj.uid }
    			}).success(function (data, status, headers, config) {
    				obj.account._id = data.account._id;
        			obj.account.mail = data.account.mail;
					obj.account.name = data.account.name;
    				obj.account.lName = data.account.lName;
    				obj.account.fName = data.account.fName;
    				obj.account.dateRegistration = data.account.dateRegistration;
    				$rootScope.$broadcast("getUser");
    			}).error(function (data, status, headers, config) {
        			console.log("No guardo el Usuario");
    				console.log(response)
    			});
    			$rootScope.$broadcast("getUser");
    		}else{
    			$rootScope.$broadcast("getUser");
    		}
    	};

        obj.getNotebooks = function(){
            if (obj.notebooksList.length > 0) {
                $rootScope.$broadcast("notebook");
            }else{
                $http({
                    url: "http://" + location.host + "/api/notebooks",
                    method: "GET",
                    headers: { 'Authorization' : obj.token , 'uid' : obj.uid }
                }).success(function (data, status, headers, config) {
                    for (i = 0; i < data.length; i++) {
                        tmp = new Notebook ();
                        angular.copy(data[i], tmp);
                        obj.notebooksList.push(tmp);
                    }
                    obj.notebooksList = data;
                    obj.getNotesList(0);
                    $rootScope.$broadcast("notebook");
                }).error(function (data, status, headers, config) {
                    console.log("No guardo el Usuario");
                    console.log(response)
                });       
            }
        };

        obj.getNotesList = function(n){
            console.log(obj.notebooksList);
            ntbook = obj.notebooksList[n];
            console.log(ntbook);
            if (ntbook.notes.length > 0) {
                $rootScope.$broadcast("notebook");
            }else{
                $http({
                    url: "http://" + location.host + "/api/notebooks/" + ntbook._id,
                    method: "GET",
                    headers: { 'Authorization' : obj.token , 'uid' : obj.uid }
                }).success(function (data, status, headers, config) {
                    obj.notebooksList[n].notes = data;
                    $rootScope.$broadcast("notebook");
                }).error(function (data, status, headers, config) {
                    console.log("No guardo el Usuario");
                    console.log(response)
                });       
            }
            
        };

        obj.getNote = function(id){
            $http({
                    url: "http://" + location.host + "/api/notes/" + id,
                    method: "GET",
                    headers: { 'Authorization' : obj.token , 'uid' : obj.uid }
                }).success(function (data, status, headers, config) {
                    console.log(data);
                    obj.lastNote = data[0];
                    $rootScope.$broadcast("note");
                }).error(function (data, status, headers, config) {
                    console.log("No guardo el Usuario");
                    console.log(response)
                })           
        };

    	return obj;
    }
);


function notebooksctrl ($scope, global){

    $scope.$on("notebook",
        function () {
            $scope.notebooks = global.notebooksList;
            console.log($scope.notebooks);
        }
    );  

    $scope.$on("login",
        function () {
            global.getNotebooks();
        }
    );

    $scope.getNotes = function(n){
        global.getNotesList(n);
    };
    $scope.getNote = function (n) {
        console.log(n);
        global.getNote(n);
    };

    $scope.nbopen = 0;

    $scope.setNbopen = function(n){
        $scope.nbopen = n;
    };

    $scope.isNopen = function(n){
        if (n == $scope.nbopen) {
            return true;
        }else{
            return false;
        }
    }
}

function userctrl($scope, global){

    $scope.$on("getUser",
        function () {
            $scope.user = global.account;
        }
    );

    $scope.$on("login",
        function () {
            global.getUser();
            $('#modal-signin').modal('hide');
        }
    );  
}

function sesionctrl($scope, $location, global) { //todo cabiarle el nombre
	$scope.createAccount = function(mail, password, firstName, lastName){
        global.createAccount(mail, password, firstName, lastName);
    };

    $scope.authentication = function(mail, password){
        //$('#login-btn').disable();
        global.login(mail, password);
    };
}

function editor($scope, global) {
	$scope.$on("note",
        function () {
            $scope.note = global.lastNote;
        }
    );
}

shots.controller('editor', editor);
shots.controller('sesionctrl', sesionctrl);
shots.controller('userctrl', userctrl);
shots.controller('notebooksctrl', notebooksctrl);
