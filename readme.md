# codeShots

A WebApp for taking programming notes. Inspired by Quiver - HappenApps

## Important Dependencies
- AngularJS
- ExpressJS
- SocketIO
- Mongoose
- MongoDB
- For more details, read package.json

## Website
(temporary) [Kodeshots](kodeshots.herokuapp.com)

## Contributors
- Jonathan Islas: [GitHub](http://github.com/islas27) | [Bitbucket](https://bitbucket.org/islas27/)
- Jesus García : [GitHub](https://github.com/chuvacagapj) | [Bitbucket](#)
- Anaiza Araiza: [GitHub](http://github.com/iza19)

## How to run locally

Install node (Preferably use [NVM](https://github.com/creationix/nvm)), then install dependencies on package.json

```sh
  $ npm install
```

Set the environment variables in a local `.env` file:
- AUTH_AUDIENCE
- AUTH_ISSUER
- AUTH_SECRET_KEY
- MONGODB_URI
- PORT

And run the following command in the terminal (Download the [heroku toolbelt](https://toolbelt.heroku.com)):

```sh
  $ heroku local:start
```

It's posible to use [foreman](http://theforeman.org) too, in place of the heroku toolbelt
