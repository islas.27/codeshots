'use strict';
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var jwt = require('jwt-simple');

var Account = require('../models/account');
var jwtconfig = require('./jwtconfig').jwtconfig;


module.exports = function (passport) {
    var opts = {};

    opts.secretOrKey = jwtconfig.secret;
    opts.issuer = jwtconfig.issuer;
    opts.audience = jwtconfig.audience;
    opts.jwtFromRequest = ExtractJwt.fromAuthHeader();

    passport.use(new JwtStrategy(opts, function (jwt_payload, done) {
        Account.findOne({mail: jwt_payload.sub}, function (err, Account) {
            if (err) {
                return done(err, false);
            }
            if (Account) {
                done(null, Account);
            }
            else done(null, false, 'Account found in token not found');
        });
    }));
};
