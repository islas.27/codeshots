module.exports.jwtconfig = {
    secret: process.env.AUTH_SECRET_KEY,
    tokenExpirationTime: 12*60*20,
    audience: process.env.AUTH_AUDIENCE,
    issuer: process.env.AUTH_ISSUER
};
