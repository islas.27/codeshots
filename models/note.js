var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Notebook = require('./notebook');

var contentBlock = new Schema({
    blockType: {
        type: String,
        required: true
    },
    blockData: {
        type: String,
        required: true
    },
    blockLanguage: {
        type: String,
        required: false
    }
});

var NoteSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    author: {
        type: Schema.Types.ObjectId,
        ref: 'Account',
        required: true
    },
    notebook: {
        type: Schema.Types.ObjectId,
        ref: 'Notebook',
        required: true
    },
    tags: [String],
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    },
    isPublic: {
        type: Boolean,
        default: false
    },
    contentList: [contentBlock]
});

NoteSchema.post('save', function (doc) {
    Notebook.findOne({'_id': doc.notebook}, function (err, notebook) {
        if (err) console.log(err);
        else {
            notebook.notes.push(doc);
            notebook.updatedAt = Date.now();
            notebook.save(function (err) {
                if (err) console.log(err);
                else {
                    console.log("note added to notebook");
                }
            })
        }
    })
});

NoteSchema.post('update', function (doc) {
    Notebook.findOne({'_id': doc.notebook}, function (err, notebook) {
        if (err) console.log(err);
        else {
            notebook.updatedAt = Date.now();
            notebook.save(function (err) {
                if (err) console.log(err);
                else {
                    console.log("notebook also updated");
                }
            })
        }
    })
});
module.exports = mongoose.model('Note', NoteSchema);
