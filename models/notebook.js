var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Account = require('./account');

var NotebookSchema = new Schema({
  name:{
    type: String,
    required: true
  },
  author:{
    type: Schema.Types.ObjectId,
    ref: 'Account',
    required: true
  },
  createdAt:{
    type: Date,
    default: Date.now
  },
  updatedAt:{
    type: Date,
    default: Date.now
  },
  isPublic:{
    type: String,
    default: false
  },
  color:{
    type: String,
    default: 'gray'
  },
  notes:[{
    type: Schema.Types.ObjectId,
    ref: 'Note',
    default: []
  }]
});

module.exports = mongoose.model('Notebook', NotebookSchema);
