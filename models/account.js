var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcryptjs');


var AccountSchema = new Schema({
    name:{
      type: String,
      required: true
    },
    lName:{
      type: String,
      required: true
    },
    fName:{
      type: String,
      required: true
    },
    dateRegistration:{
      type: Date,
      default: Date.now
    },
    mail: {
        type: String,
        unique: true,
        required: true,
        validate: function(mail) {
          return /^[a-zA-Z0-9.!#$%&’*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(mail)
        }
    },
    password: {
        type: String,
        required: true
    },
    isActive: {
        type: Boolean,
        default: true
    }
});

AccountSchema.pre('save', function (next) {
    var account = this;
    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, function (err, salt) {
            if (err) {
                return next(err);
            }
            bcrypt.hash(account.password, salt, function (err, hash) {
                if (err) {
                    return next(err);
                }
                account.password = hash;
                next();
            });
        });
    } else {
        return next();
    }
});


AccountSchema.methods.comparePassword = function (passw, cb) {
    bcrypt.compare(passw, this.password, function (err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    })
};

module.exports = mongoose.model('Account', AccountSchema);
