var express = require('express');
var router = express.Router();
var passport = require('passport');
var Account = require('../models/account');
var Notebook = require('../models/notebook');
var Note = require('../models/note');
var jwtconfig = require('../config/jwtconfig').jwtconfig;
var config = require('../config/database').database;
var jwt = require('jwt-simple');
var mongoose = require('mongoose');

router.get('/myaccount', function (req, res) {
  var encodedToken = req.headers.authorization.split(' ');
  var token = jwt.decode(encodedToken[1], jwtconfig.secret);
    getAccountPromise(req.headers)
    .then(function (account) {
        res.status(200).json({account, token});
    })
    .onReject(function (err) {
        res.status(403).json({msg:err});
    });
});

router.post('/notebooks', function (req, res) {
    if (!req.body.name && !req.get('uid')) {
        res.status(400).json({success: false, msg: "Send the required data."});
    } else {
      var newNotebook = new Notebook({
          name: req.body.name,
          author: req.get('uid')
      });
      newNotebook.save()
      .then(function(notebook){
        res.status(201).json({success: true, msg: 'Notebook created Successfully.', notebook: notebook});
      })
      .onReject(function(err) {
        res.status(400).json({msg:err});
      });
    }
});

router.get('/notebooks', function (req, res) {
  var searchQuery = {author: req.get('uid')};
  var selectQuery = 'name createdAt updatedAt isPublic color notes';
  var populateQuery = { path: 'notes', select: '_id, title', options: { sort:'-updatedAt' }};
  getNotebookPromise(searchQuery, selectQuery, populateQuery)
  .then(function(notebooks){
    res.status(200).json(notebooks);
  })
  .onReject(function(err){
    res.status(400).json({msg:err});
  });
});

router.get('/notebooks/:id', function (req, res) {
  var searchQuery = {_id: req.params.id};
  var selectQuery = 'name createdAt updatedAt isPublic color notes';
  var populateQuery = { path: 'notes', select: '_id, title', options: { sort:'-updatedAt' }};
  getNotebookPromise(searchQuery, selectQuery, populateQuery)
  .then(function(notebook){
    res.status(200).json(notebook);
  })
  .onReject(function(err){
    res.status(400).json({msg:err});
  });
});

router.put('/notebooks/:id', function (req, res) {
  if(!(req.get('Content-Type') == 'application/json') || !req.body){
    res.status(400).json({success:false,msg:"No data sent or incorrect req"});
  }else {
    var notebook = req.body;
    var updatedData = {
      name:notebook.name,
      updatedAt:Date.now(),
      isPublic:notebook.isPublic,
      color:notebook.color,
      notes:notebook.notes
    }
    Notebook.findByIdAndUpdate(notebook._id, updatedData, function(err){
      if(!err) res.status(200).json({success:true, msg:"Notebook updated Successfully"});
      else res.status(400).json({success:false,msg:err});
    });
  }
});

router.delete('/notebooks/:id', function (req, res) {
  var searchQuery = {_id: req.params.id, author: req.get('uid')};
  Notebook.findOneAndRemove(searchQuery, function (err, doc) {
    if(err) res.status(400).json({success:false,msg:"Error: " + err});
    else{
      console.log("Removed Notebook " + doc._id + " of user: " + req.get('uid'));
      res.status(200).json({success:true,msg:"Notebook deleted Successfully"});
    }
  })
});

router.post('/notes', function (req, res) {
  if(!req.body.title && !req.body.notebook){
    res.status(400).json({success:false, msg:"Send complete data"});
  }else{
    var newNote = new Note({
        title: req.body.title,
        author: req.get('uid'),
        notebook: req.body.notebook
    });
    newNote.save()
    .then(function (note) {
      res.status(201).json({success:true, msg:"Note created Successfully", note:note});
    })
    .onReject(function (err) {
      res.status(400).json({success:false,msg:err});
    });
  }
});

router.get('/notes/:id', function (req, res) {
  if (!req.params.id) {
    res.status(400).json({msg:"No id present in the request"});
  } else{
    var searchQuery = {_id:req.params.id};
    getNotePromise(searchQuery)
    .then(function (note) {
      res.status(200).json(note);
    })
    .onReject(function (err) {
      res.status(400).json({msg:err});
    });
  }
});

router.get('/notes/tags/:tag', function (req, res) {
  if (!req.params.tag) {
    res.status(400).json({msg:"No tag present in the request"});
  } else{
    var searchQuery = {tags:req.params.tag, author:req.get('uid')};
    var selectQuery = "_id title createdAt updatedAt";
    getNotePromise(searchQuery, selectQuery)
    .then(function (notes) {
      res.status(200).json(notes);
    })
    .onReject(function (err) {
      res.status(400).json({msg:err});
    });
  }
});

router.put('/notes/:id', function (req, res) {
  if(!(req.get('Content-Type') == 'application/json') || !req.body){
    res.status(400).json({success:false,msg:"No data sent or incorrect req"});
  }else {
    var note = req.body;
    var updatedData = {
      title:note.title,
      updatedAt:Date.now(),
      isPublic:note.isPublic,
      tags:note.tags,
      contentList:note.contentList
    }
    Note.findByIdAndUpdate(note._id, updatedData, function(err){
      if(!err) res.status(200).json({success:true, msg:"Note updated Successfully"});
      else res.status(400).json({success:false,msg:err});
    });
  }
});

router.delete('/notes/:id', function (req, res) {
  if (!req.params.id) {
    res.status(400).json({msg:"No id present in the request"});
  } else{
    var searchQuery = {_id:req.params.id};
    Note.findOneAndRemove(searchQuery, function(err, doc) {
      if(!err) res.status(200).json({success:true, msg:"Note delted Successfully"});
      else res.status(400).json({success:false,msg:"An error ocurred"})
    });
  }
});

var getAccountPromise = function (headers) {
    var encodedToken = headers.authorization.split(' ');
    var token = jwt.decode(encodedToken[1], jwtconfig.secret);
    var searchQuery = {mail: token.sub};
    var selectQuery = 'name lName fName dateRegistration mail';
    return Account.findOne(searchQuery).select(selectQuery).exec();
};

var getNotebookPromise = function (searchQuery, selectQuery, populateQuery) {
  var promise = Notebook.find(searchQuery).sort('-updatedAt');
  if(selectQuery) promise.select(selectQuery);
  if(populateQuery) promise.populate(populateQuery);
  return promise.exec();
};

var getNotePromise = function (searchQuery, selectQuery, populateQuery) {
  var promise = Note.find(searchQuery);
  if(selectQuery) promise.select(selectQuery);
  if(populateQuery) promise.populate(populateQuery);
  return promise.exec();
};

module.exports = router;
