var express = require('express');
var router = express.Router();
var Account = require('../models/account');
var jwtconfig = require('../config/jwtconfig').jwtconfig;
var config = require('../config/database').database;
var jwt = require('jwt-simple');
var mongoose = require('mongoose');

mongoose.connect(config.source);

router.post('/signup', function (req, res) {
    if (!(req.body.mail && req.body.password && req.body.firstName && req.body.lastName)) {
        res.status(400).json({success: false, msg: req.body});
    } else {
        var newAccount = new Account({
            mail: req.body.mail,
            password: req.body.password,
            name: req.body.firstName,
            lName: req.body.lastName,
            fName: req.body.firstName + " " + req.body.lastName,
        });
        console.log(newAccount);
        newAccount.save(function (err) {
            if (err) {
                console.log(err);
                return res.status(400).json({success: false, msg: err});
            }
            res.status(201).json({success: true, msg: 'Successfully created an Account!'});
        })
    }
});

router.post('/authenticate', function (req, res) {
    Account.findOne({mail: req.body.mail}, function (err, account) {
        if (err) throw err;
        if (!account) {
            res.status(401).send({msg: 'Authentication failed. Account not found'});
        } else {
            account.comparePassword(req.body.password, function (err, isMatch) {
                if (isMatch && !err) {
                    var iat = new Date().getTime() / 1000;
                    var exp = iat + jwtconfig.tokenExpirationTime;
                    var payload = {
                        aud: jwtconfig.audience,
                        iss: jwtconfig.issuer,
                        iat: iat,
                        exp: exp,
                        sub: account.mail
                    };
                    var token = jwt.encode(payload, jwtconfig.secret);
                    res.json({token: 'JWT ' + token, uid: account._id});
                } else {
                    res.status(401).send({msg: 'Authentication failed. Wrong password'});
                }
            })
        }

    })
});

module.exports = router;
